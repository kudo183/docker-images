#!/bin/bash
arch=$(uname -m)
echo "arch: $arch"
if [ "$arch" = "aarch64" ]
then
  docker build --no-cache -t kudo183/flutter-linux-web:arm64 .
  docker push kudo183/flutter-linux-web:arm64
else
  docker build --no-cache -t kudo183/flutter-linux-web:x64 .
  docker push kudo183/flutter-linux-web:x64
fi