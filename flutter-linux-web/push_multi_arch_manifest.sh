#!/bin/bash
echo "required kudo183/flutter-linux-web:x64, kudo183/flutter-linux-web:arm64 already pushed"
docker manifest rm kudo183/flutter-linux-web
docker manifest create kudo183/flutter-linux-web --amend kudo183/flutter-linux-web:x64 --amend kudo183/flutter-linux-web:arm64
docker manifest push kudo183/flutter-linux-web