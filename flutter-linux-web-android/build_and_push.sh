#!/bin/bash
arch=$(uname -m)
echo "arch: $arch"
if [ "$arch" = "aarch64" ]
then
  echo "flutter android on arm64 not work yet"
  # docker build --no-cache -t kudo183/flutter-linux-web-android:arm64 .
  # docker push kudo183/flutter-linux-web-android:arm64
else
  docker build --no-cache -t kudo183/flutter-linux-web-android:x64 .
  docker push kudo183/flutter-linux-web-android:x64
fi